---
layout: handbook-page-toc
title: "RFP Process - Field Security"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}


## RFP Process

The Field Security Team has created a process to assist and simplify the RFP process for the Field Teams. We will utilize our [current submission process](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) and our tool [Vendorpedia](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/Vendorpedia.html) (by OneTrust) to collaborate cross-departmentally with the required SME's and expedite completion. 


### Submission

Use the [Main Template](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/customer-assurance-activities/caa-servicedesk/-/issues/new?issuable_template=main_template) on the Field Security Issue board and title it 'RFP - Customer'. 
Please be sure to include all requested information. 

Our team will then input the RFP into Vendorpedia and run the automated completion as a first pass. Any questions unable to be answered will be assigned to relevant SME's and completed within SLA (10 days). 


### Completion

Once completed, our team will return the completed document to the person who submitted the issue along with any relevant documentation. 

`Please note` that an NDA may be required for some documentation (items listed in our Customer version of the CAP). 
