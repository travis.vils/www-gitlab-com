---
layout: markdown_page
title: "Talent Acquisition Alignment"
description: "This page is an overview of the search team alignment and the talent acquisition platform directly responsible individual in talent acquisition operations and talent brand."
---

## Search Team Alignment by Department

| Department                    | Recruiter       | Sourcer     | Candidate Experience Specialist    |
|--------------------------|-----------------|-----------------|-------------------------------------|
| Executive          | Rich Kahn   | Chriz Cruz | Davie Soomalelagi  |
| Enterprise Sales, NA | Kevin Rodrigues |  N/A | Sruthy Menon |
| Enterprise Sales, NA | Jill Hind |  N/A | Sruthy Menon |
| Enterprise Sales, EMEA | Debbie Harris |  TBH | Lerato Thipe |
| Commercial Sales,	AMER | Marcus Carter | TBH  | Davie Soomalelagi |
| Commercial Sales,	AMER | Hannah Stewart | TBH  | Sruthy Menon |
| Commercial Sales,	EMEA | Ben Cowdry | TBH | Lerato Thipe |
| Channel Sales, US/EMEA | Debbie Harris & Kanwal Matharu | TBH | Lerato Thipe |
| Field Operations,	US/EMEA | Kelsey Hart | Loredana Iluca | Sruthy Menon |
| Customer Success, EMEA | Joanna Muttiah & Ornella Gerca | Loredana Iluca | Lerato Thipe |
| Customer Success, NA | Barbara Dinoff | Loredana Iluca | Sruthy Menon |
| All Sales, APAC | Yas Priatna | N/A | Lerato Thipe |
| Marketing, Global | Steph Sarff   | Alina Moise | Davie Soomalelagi |
| Marketing, SDR Global | Tony Tsiras | Tony Tsiras| Alice Crosbie |
| G&A, Finance, People, CEO | Maria Gore | Alina Moise | Alice Crosbie |
| G&A, Accounting, Legal | Rachelle Druffel | Alina Moise | Davie Soomalelagi |
| Development: AML/ModelOps, Fulfillment, Enablement | Mark Deubel| Zsuzsanna Kovacs and Susan Hill | Guido Rolli |
| Development: Ops | Riley Smith | Zsuzsanna Kovacs and Susan Hill | Guido Rolli |
| Development: Dev | Sara Currie | Zsuzsanna Kovacs and Susan Hill | Guido Rolli |
| Development: Analytics, Growth, Secure | Dielle Kuffel | Zsuzsanna Kovacs and Susan Hill | Guido Rolli |
| Quality | Rupert Douglas| Zsuzsanna Kovacs | Guido Rolli |
| Quality | Matt Angell   | Zsuzsanna Kovacs | Alice Crosbie |
| UX  | Rupert Douglas   | Zsuzsanna Kovacs  | Guido Rolli |
| Support | Joanna Michniewicz  |  Joanna Michniewicz | Alice Crosbie |
| Security | Nicky Kunstman |  Zsuzsanna Kovacs | Michelle Jubrey |
| Incubation | Matt Angell  |  Zsuzsanna Kovacs | Alice Crosbie |
| Infrastructure   | Josh Barker  | Susan Hill | Michelle Jubrey |
| Product Management  | Matt Allen | Chris Cruz | Michelle Jubrey |

For urgent requests of the Candidate Experience Specialist team, we encourage you to contact them by also tagging @CES in Slack messages and CC'ing CES@gitlab.com on emails. 

## Inbound Sourcing Alignment

| Department                 | Sourcer     |
|--------------------------|-----------------|
| R&D        | Anchal Sharma  | 
| Sales/ G&A        | Priya Chokanda   |
| Marketing/ G&A        | Muskan Mehta   |

## Talent Acquisition Leader Alignment

| Department                    | Leader      | 
|--------------------------|-----------------|
| Talent Acquisition         | Rob Allen |
| Talent Brand and Enablement | Devin Rogozinski |
| Talent Acquisition (Sales) | Jake Foster|
| Talent Acquisition (Marketing) | Jake Foster |
| Talent Acquisition (G&A) | Jake Foster |
| Talent Acquisition (R&D) | Ursela Knezevic |
| Candidate Experience & Enablement | Marissa Ferber |
| Inbound Sourcing | Chris Cruz |

## Talent Acquisition Platform Directly Responsible Individual

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Devin Rogozinski/Marissa Ferber |
| Comparably | Content Management | Devin Rogozinski |
| Glassdoor | Admin  | Devin Rogozinski |
| Glassdoor | Responding to Reviews  | Devin Rogozinski |
| Glassdoor | Content Management | Devin Rogozinski |
| LinkedIn | Admin - Recruiter  | Devin Rogozinski |
| LinkedIn | Seats | Devin Rogozinski |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Talent Acquisition | Devin Rogozinski |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Devin Rogozinski |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |
